package de.benoty.antiafk.utils;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.List;

public class ItemBuilder 
{

    // -- instances 
    
    private final ItemStack itemStack;

    // -- (overloaded) constructor
    
    /**
     * this constructor creates a new ItemStack
     * @param material - the material which should be the ItemStack
     */
    public ItemBuilder(Material material) 
    {
        itemStack = new ItemStack(material);
    }

    /**
     * this constructor creates a new ItemStack
     * @param material - the material which should be the ItemStack
     * @param data - the material data
     */
    public ItemBuilder(Material material, byte data) 
    {
        itemStack = new ItemStack(material, 1, data);
    }
    
    // -- public methods

    /**
     * this method sets the displayname of the itemstack
     * @param name - displayname 
     * @return itemstack with the displayname
     */
    public ItemBuilder withName(String name) 
    {
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName(name);
        itemStack.setItemMeta(itemMeta);
        return this;
    }

    /**
     * this methods sets the lore for an itemstack
     * @param lore which should be displayed
     * @return 
     */
    public ItemBuilder withLore(List<String> lore) 
    {
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.setLore(lore);
        itemStack.setItemMeta(itemMeta);
        return this;
    }

    /**
     * this method sets the SkullOwner for an ItemStack
     * @param owner - the name of the head to be used as a template
     * @return
     */
    public ItemBuilder withSkull(String owner) 
    {
        SkullMeta itemMeta = (SkullMeta)itemStack.getItemMeta();
        itemMeta.setOwner(owner);
        itemStack.setItemMeta(itemMeta);
        return this;
    }

    /**
     * this method builds the ItemStack
     * @return itemstack 
     */
    public ItemStack build() 
    {
        return this.itemStack;
    }
}

package de.benoty.antiafk;

import de.benoty.antiafk.command.SetKickStateCommand;
import de.benoty.antiafk.command.ToggleAfkKickStateCommand;
import de.benoty.antiafk.listener.ChatPingListener;
import de.benoty.antiafk.listener.StartAfkCheckListener;
import de.benoty.antiafk.utils.KickState;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class AntiAFK extends JavaPlugin {

    private static AntiAFK instance;

    @Override
    public void onEnable() {
        final long epoch = System.currentTimeMillis();
        Bukkit.getLogger().info("Initializing antiafk plugin...");

        AntiAFK.setKickState(KickState.AFK_MODE);

        instance = this;

        this.loadConfig();
        this.initCommands();
        this.initListener();
        this.initVariables();

        final long initializeDuration = System.currentTimeMillis() - epoch;
        Bukkit.getLogger().info(String.format("Successfully initialized antiafk plugin. (took %sms)", initializeDuration));
    }

    @Override
    public void onDisable() {
    }

    private File file;
    private YamlConfiguration config;

    private void loadConfig() {
        if (!getDataFolder().exists()) {
            getDataFolder().mkdirs();
        }

        file = new File(getDataFolder(), "config.yml");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        config = YamlConfiguration.loadConfiguration(file);
        config.options().copyDefaults(true);
        config.addDefault("prefix", "&cAntiAFK &8» &7");
        config.addDefault("usagePrefix", "&4Verwendung &8» &c");
        config.addDefault("kickString", "&4&lWARNUNG \n &8⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊ \n &7Bitte sei als Teammitglied nicht &cabwesend&7!");

        try {
            config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static List<UUID> antiKickUserList;
    private static List<UUID> afkUserList;
    private String prefix;
    private String usagePrefix;
    private String kickString;
    public void initVariables() {
        antiKickUserList = new ArrayList<>();
        afkUserList = new ArrayList<>();
        prefix = ChatColor.translateAlternateColorCodes('&', this.getConfig().getString("prefix"));
        usagePrefix = ChatColor.translateAlternateColorCodes('&', this.getConfig().getString("usagePrefix"));
        kickString = ChatColor.translateAlternateColorCodes('&', this.getConfig().getString("kickString"));
    }

    private void initCommands() {
        Bukkit.getPluginCommand("toggleafkkick").setExecutor(new ToggleAfkKickStateCommand());
        Bukkit.getPluginCommand("changekickstate").setExecutor(new SetKickStateCommand());
    }

    private void initListener() {
        PluginManager pluginManager = Bukkit.getPluginManager();

        pluginManager.registerEvents(new ChatPingListener(), this);
        pluginManager.registerEvents(new StartAfkCheckListener(), this);
    }

    public static KickState kickState;

    public static void setKickState(KickState kickState) {
        AntiAFK.kickState = kickState;
    }

    public static KickState getKickState() {
        return kickState;
    }

    public static AntiAFK getInstance() {
        return instance;
    }

    public static List<UUID> getAfkUserList() {
        return afkUserList;
    }

    public static List<UUID> getAntiKickUserList() {
        return antiKickUserList;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getUsagePrefix() {
        return usagePrefix;
    }

    public String getKickString() {
        return kickString;
    }

}

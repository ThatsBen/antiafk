package de.benoty.antiafk.listener;

import de.benoty.antiafk.utils.KickState;
import de.benoty.antiafk.AntiAFK;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.HashMap;
import java.util.UUID;

public class StartAfkCheckListener implements Listener
{

    private final HashMap<UUID, Long> timeMove = new HashMap<>();

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        if (player.isOp()) {
            timeMove.put(player.getUniqueId(), System.currentTimeMillis());
            startAfkCheck(player);
        }
    }

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        timeMove.put(player.getUniqueId(), System.currentTimeMillis());
        AntiAFK.getAfkUserList().remove(player.getUniqueId());
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        AntiAFK.getAfkUserList().remove(player.getUniqueId());

        if (running) {
            this.cancelTask();
        }
    }

    /**
     * Boolean if task run
     */
    private boolean running = false;

    /**
     * The bukkit task
     */
    private BukkitTask task;

    /**
     * Start the afk check
     *
     * @param player The player where the check is performed
     */

    public void startAfkCheck(Player player) {
        this.running = true;
        task = new BukkitRunnable() {
            @Override
            public void run() {
                long current = System.currentTimeMillis();
                long last = timeMove.getOrDefault(player.getUniqueId(), 0L);

                long diff = current - last;
                if (diff > 30000 /* 30 Seconds */
                        && !AntiAFK.getAfkUserList().contains(player.getUniqueId()) && AntiAFK.getAfkUserList().contains(player.getUniqueId())) {
                    // player don't move
                    AntiAFK.getAfkUserList().add(player.getUniqueId());
                    player.sendMessage(AntiAFK.getInstance().getPrefix() + "§cDu wurdest als abwesend markiert.");
                }
                if (diff > 60000 /* 60 seconds*/
                        && !AntiAFK.getAntiKickUserList().contains(player.getUniqueId())) {
                    // player don't move and is already afk
                    if (AntiAFK.getKickState().equals(KickState.SERVER_KICK)) {
                        player.kickPlayer(AntiAFK.getInstance().getKickString());
                    }

                    if (AntiAFK.getKickState().equals(KickState.AFK_MODE)) {
                        player.sendMessage(AntiAFK.getInstance().getPrefix() + "Du bist nun AFK.");
                        AntiAFK.getAfkUserList().add(player.getUniqueId());
                        this.cancel();
                    }

                }
            }
        }.runTaskTimer(AntiAFK.getInstance(), 0, 20L);
    }

    /**
     * Cancel the task
     */
    private void cancelTask() {
        this.running = false;
        this.task.cancel();
    }

    private BukkitTask getTask() {
        return task;
    }

}

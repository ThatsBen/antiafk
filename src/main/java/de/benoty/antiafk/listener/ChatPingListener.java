package de.benoty.antiafk.listener;

import de.benoty.antiafk.AntiAFK;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatPingListener implements Listener {

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        String message = event.getMessage();
        Player player = event.getPlayer();
        for (Player players : Bukkit.getOnlinePlayers()) {
            if (message.contains(players.getDisplayName())) {
                if (AntiAFK.getAfkUserList().contains(players.getUniqueId())) {
                    players.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);
                    player.sendMessage(AntiAFK.getInstance().getPrefix() + "§cAchtung, §r" + players.getDisplayName() + " §cist derzeit §c§lAFK / Nicht erreichbar §cund wird dir deshalb eventuell nicht antworten.");
                }
            }
        }
    }
}

package de.benoty.antiafk.listener;

import de.benoty.antiafk.AntiAFK;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class QuitListener implements Listener {

    /**
     * If the player leaves the server, he will be removed from the AFK User List
     * @param event
     */
    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        if (AntiAFK.getAfkUserList().contains(player.getUniqueId())) {
            AntiAFK.getAfkUserList().remove(player.getUniqueId());
        }
    }
}

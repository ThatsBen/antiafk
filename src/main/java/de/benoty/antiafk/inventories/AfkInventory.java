package de.benoty.antiafk.inventories;

import de.benoty.antiafk.AntiAFK;
import de.benoty.antiafk.utils.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

public class AfkInventory implements Listener 
{

    /**
     * this method creates an inventory
     * @param player - player who opens the inventory
     * @return inventory
     */
    public static Inventory get(Player player) 
    {
        Inventory inventory = Bukkit.createInventory(null, 9*3, ChatColor.DARK_GRAY + "» AFK-GUI");

        for (int i = 0; i < inventory.getSize(); i++) 
        {
            inventory.setItem(i, new ItemBuilder(Material.STAINED_GLASS_PANE, (byte) 7).withName(" ").build());
        }

        if (player.hasPermission("admin.afkgui.command")) 
        {
            inventory.setItem(4, new ItemBuilder(Material.LEVER).withName(ChatColor.GRAY + "Plugin" + ChatColor.GREEN + " aktivieren " + ChatColor.GRAY + " / " + ChatColor.RED + "deaktivieren").build()); 
        }

        inventory.setItem(11, new ItemBuilder(Material.WOOL, (byte) 5).withName(ChatColor.GREEN + "AFK-Kick aktivieren").build());
        inventory.setItem(15, new ItemBuilder(Material.WOOL, (byte) 14).withName(ChatColor.RED + "AFK-Kick deaktivieren").build());

        inventory.setItem(26, new ItemBuilder(Material.BARRIER).withName(ChatColor.RED + "Inventar schließen").build());

        return inventory;
    }

    /**
     * This method contains the logic for the interactions in the inventory
     * @param event
     */
    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) 
    {
        Player player = (Player) event.getWhoClicked();
        if (event.getView().getTitle().equals(ChatColor.DARK_GRAY + "» AFK-GUI"))
        {
            event.setCancelled(true);
            if (player.hasPermission("admin.afkgui.command")) // admin tool
            {
                player.sendMessage(AntiAFK.getInstance().getPrefix() + ChatColor.RED + "Noch in Entwicklung...");
                player.closeInventory();
            }

            if (event.getSlot() == 11) // antikick activate
            {
                if (!AntiAFK.getAntiKickUserList().contains(player.getUniqueId())) 
                {
                    AntiAFK.getAntiKickUserList().add(player.getUniqueId());
                    player.sendMessage(AntiAFK.getInstance().getPrefix() + ChatColor.GRAY + "Du wirst nun" + ChatColor.RED + " wieder " + ChatColor.GRAY + "gekickt.");
                } 
                else 
                {
                    player.sendMessage(AntiAFK.getInstance().getPrefix() + ChatColor.GRAY + "Du" + ChatColor.RED +" wirst " + ChatColor.GRAY + " bereits bei Inaktivität vom Netzwerk gekickt.");
                }
                player.closeInventory();
            }

            if (event.getSlot() == 15) // antikick deactivate
            {
                if (AntiAFK.getAntiKickUserList().contains(player.getUniqueId())) 
                {
                    AntiAFK.getAntiKickUserList().remove(player.getUniqueId());
                    player.sendMessage(AntiAFK.getInstance().getPrefix() + ChatColor.GRAY + "Du wirst nun" + ChatColor.GREEN + " nicht mehr" + ChatColor.GRAY +" gekickt!");
                } 
                else 
                {
                    player.sendMessage(AntiAFK.getInstance().getPrefix() + ChatColor.GRAY + "Du wirst bereits " + ChatColor.GREEN + "nicht mehr " + ChatColor.GRAY + "bei Inaktivität gekickt.");
                }
                player.closeInventory();
            }
            
            if (event.getSlot() == 26) // barrier 
            {
                player.closeInventory();
            }
        }
    }
    
}

package de.benoty.antiafk.command;

import de.benoty.antiafk.AntiAFK;
import de.benoty.antiafk.inventories.AfkInventory;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class AfkGuiCommand implements CommandExecutor {

    /**
     * This command opens the AFK GUI
     */
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("");
            return true;
        }

        Player player = (Player) sender;

        if (args.length > 0) {
            player.sendMessage(AntiAFK.getInstance().getUsagePrefix() + "§c/afkgui");
            return true;
        }

        player.openInventory(AfkInventory.get(player));

        return true;
    }
}

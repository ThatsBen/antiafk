package de.benoty.antiafk.command;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.benoty.antiafk.AntiAFK;

public class ToggleAFKCommand implements CommandExecutor {

    /**
     * 
     */
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(AntiAFK.getInstance().getPrefix() + ChatColor.RED + "Du musst ein Spieler sein!");
            return true;
        }

        Player player = (Player) sender;
        if (!player.hasPermission("toggleafk.command")) {
            this.sendNoPerms(player);
            return true;
        }

        if (args.length > 0) {
            player.sendMessage(AntiAFK.getInstance().getUsagePrefix() + ChatColor.RED + "/toggleafk");
            return true;
        }

        if (!AntiAFK.getAntiKickUserList().contains(player.getUniqueId())) {
            AntiAFK.getAntiKickUserList().add(player.getUniqueId());
            player.sendMessage(AntiAFK.getInstance().getPrefix() + ChatColor.GRAY + "Du wirst nun " + ChatColor.GREEN + "nicht mehr " + ChatColor.GRAY + "gekickt!");
            return true;
        }
        else if (AntiAFK.getAntiKickUserList().contains(player.getUniqueId())) {
            AntiAFK.getAntiKickUserList().remove(player.getUniqueId());
            player.sendMessage(AntiAFK.getInstance().getPrefix() + ChatColor.GRAY + "Du wirst nun " + ChatColor.RED + "wieder " + ChatColor.GRAY + "gekickt!");
        }
        return false;
    }

    public void sendNoPerms(final Player player) {
        player.sendMessage(AntiAFK.getInstance().getPrefix() + ChatColor.RED + "Dazu hast du keine Berechtigung!");
    }

}

package de.benoty.antiafk.command;

import de.benoty.antiafk.utils.KickState;
import de.benoty.antiafk.AntiAFK;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetKickStateCommand implements CommandExecutor 
{

    /**
     * This command lets an admin decide if the players will be kicked or just made invisible
     */
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) 
    {
        if (!(sender instanceof Player)) 
        {
            sender.sendMessage(AntiAFK.getInstance().getPrefix() + ChatColor.RED + "Du musst ein Spieler sein!");
            return true;
        }
        Player player = (Player) sender;

        if (!player.hasPermission("admin.setkickstate")) 
        {
            player.sendMessage(AntiAFK.getInstance().getPrefix() + ChatColor.RED + "Dazu hast du keine Rechte.");
            return true;
        }

        if (args.length > 0) 
        {
            player.sendMessage(AntiAFK.getInstance().getUsagePrefix() + ChatColor.RED + "/changekickstate");
            return true;
        }

        if (AntiAFK.getKickState().equals(KickState.SERVER_KICK))
        {
            AntiAFK.setKickState(KickState.AFK_MODE);
            player.sendMessage(AntiAFK.getInstance().getPrefix() + ChatColor.GRAY + "Die Spieler werden nun nicht mehr gekickt.");
            return true;
        }

        if (AntiAFK.getKickState().equals(KickState.AFK_MODE)) 
        {
            AntiAFK.setKickState(KickState.SERVER_KICK);
            player.sendMessage(AntiAFK.getInstance().getPrefix() + ChatColor.GRAY + "Die Spieler werden nun vom Server gekickt.");
            return true;
        }
        return false;
    }
}

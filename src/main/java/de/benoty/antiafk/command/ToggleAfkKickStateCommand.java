package de.benoty.antiafk.command;

import de.benoty.antiafk.AntiAFK;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ToggleAfkKickStateCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(AntiAFK.getInstance().getPrefix() + "§cDu musst ein Spieler sein!");
            return true;
        }

        Player player = (Player) sender;
        if (!player.hasPermission("toggleafk.command")) {
            player.sendMessage(AntiAFK.getInstance().getPrefix() + "§cDazu hast du keine Berechtigung!");
            return true;
        }

        if (args.length > 0) {
            player.sendMessage(AntiAFK.getInstance().getUsagePrefix() + "§c/toggleafk");
            return true;
        }

        // Check if the player is already in the anti kick list
        // If he is already in the list, he should not be kicked for inactivity anymore
        if (AntiAFK.getAntiKickUserList().contains(player.getUniqueId())) {
            AntiAFK.getAntiKickUserList().remove(player.getUniqueId()); // The player will now be kicked from the server again for inactivity
            player.sendMessage(AntiAFK.getInstance().getPrefix() + "§7Du wirst nun §cwieder §7gekickt!");
        } else {
            // If the player is not in the anti kick list yet, he will be added
            AntiAFK.getAntiKickUserList().add(player.getUniqueId());  // The player will no longer be kicked due to inactivity
            player.sendMessage(AntiAFK.getInstance().getPrefix() + "§7Du wirst nun §anicht mehr §7gekickt!");
        }
        return true;
    }

}
